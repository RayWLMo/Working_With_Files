# print(1/0)  # ZeroDivisionError: division by zero
#
# num = 9
# if num > 8  # No colon for if condition results in a SyntaxError: invalid syntax
#     print(num)
#
# # Creating a file with required permission and see what errors/exception are possible to be seen
#
# #First Iteration
# file = open("order.txt")  # open() takes a string arg with file name
# print(file)  # Outcome and record of the file
# # FileNotFoundError: [Errno 2] No such file or directory: 'order.text'
#
# # Second Iteration
# try:
#     file = open("order.txt")
#     print("The file was found")  # try block requires except or will give error
# except FileNotFoundError as errmsg:  # Creating an Alias - alternate name for the error
#     print(f"Uh oh. There is no file: {errmsg}")
#
# finally:  # finally will execute regardless of try and except block execution, also used to clean up the code
#     print("Thank you for using this program")
#
# # Creating the file to be used with exceptions
# # - Naming convention is essential
#
# # Applying DRY, OOP and Python Packaging
#         #   1    2             3


class EditFiles:
    def File_Check(self):
        try:
            file = open("order.txt")
            print("The file was found")
        except FileNotFoundError as errmsg:
            print(f"Uh oh, there is no file: {errmsg}")
        finally:
            print("Thank you for using this program")


