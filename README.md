# Working With Files
## Exception Handling
### File permissions

#### Examples of errors/exceptions
- `ValueError`
- `SyntaxError`
- `Out of bounds`
- `KeyError`
- `AttributeError`
- `ZeroDivisionError`
- `NameError`

```python
print(1/0)  # ZeroDivisionError: division by zero

num = 9
if num > 8  # No colon for if condition results in a SyntaxError: invalid syntax
    print(num)
```

#### File Permissions

- `r` - This is the default mode. It opens the file for reading
- `w` - This mode opens file for writing. If the file doesn't exist, it creates a new file
- `x` - Creates a new file, if already exists, the operation fails
- `a` Opens the file in Append mode, if file doesn't exist, a new file will be created
- `t` - This is the default mode to open in text mode
- `b` - This is for binary mode
- `+` - This will open a file for reading and writing (generally used for updating files)

### `try` `except` and `finally`
- `try` works as `if` condition
-`except` works as `elif` condition
- `finally` works as `else`, it will execute regardless of `try` and `except` conditions

### Creating a file with required permission and see what errors/exception are possible to be seen

####First Iteration
```python
file = open("order.txt")  # open() takes a string arg with file name
print(file)  # Outcome and record of the file
# FileNotFoundError: [Errno 2] No such file or directory: 'order.text'
```

#### Second Iteration
```python
try:
    file = open("order.txt")
    print("The file was found")  # try block requires except or will give error
except FileNotFoundError as errmsg:  # Creating an Alias - alternate name for the error
    print(f"Uh oh. There is no file: {errmsg}")

finally:  # finally will execute regardless of try and except block execution, also used to clean up the code
    print("Thank you for using this program")
```

#### Third Iteration - Creating the file to be used with exceptions

**Naming convention is essential**

Applying DRY, OOP and Python Packaging
```python
class EditFiles:
    def File_Check(self):
        try:
            file = open("order.txt")
            print("The file was found")
        except FileNotFoundError as errmsg:
            print(f"Uh oh, there is no file: {errmsg}")
        finally:
            print("Thank you for using this program")
```
